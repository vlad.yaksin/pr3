<?php

namespace Fastfood;

class Food
{
    public $name;

    function __construct($name)
    {
        $this->name = $name;
    }
}
class Burger extends Food
{
    public $pickles = true;
    public $onion = true;
    function __construct($name, $pickles, $onion)
    {
        parent::__construct($name);
        $this->pickles = $pickles;
        $this->onion = $onion;
    }
    function displayInfo()
    {
        echo "$this->name ";
        if ($this->pickles == true)
        {
            echo "c огурцами и ";
        }
        else
        {
            echo "без огурцов и ";
        }
        if ($this->onion == true)
        {
            echo "c луком<br>";
        }
        else
        {
            echo "без лука<br>";
        }
    }
}
class Potato extends Food
{
    public $sauce = true;
    public $salt = true;
    function __construct($name, $salt, $sauce)
    {
        parent::__construct($name);
        $this->salt = $salt;
        $this->sauce = $sauce;
    }
    function displayInfo()
    {
        echo "Картошка-фри ";
        if ($this->salt == true)
        {
            echo "c солью и ";
        }
        else
        {
            echo "без соли и ";
        }
        if ($this->sauce == true)
        {
            echo "c соусом<br>";
        }
        else
        {
            echo "без соуса<br>";
        }
    }
}
class Coffee extends Food
{
    public $sugar = true;
    function __construct($name, $sugar)
    {
        parent::__construct($name);
        $this->sugar = $sugar;
    }
    public function displayInfo()
    {
        echo "$this->name ";
        if ($this->sugar == true)
        {
            echo "c сахаром<br>";
        }
        else
        {
            echo "без сахара<br>";
        }
    }
}

class Personal
{
    public $name;
    function __construct($name)
    {
        $this->name = $name;
    }
}
class Cook extends Personal
{
    function __construct($name)
    {
        parent::__construct($name);
    }
    public function Order(Operator $operator)
    {
        echo "Повар $this->name получает заказ от оператора $operator->name<br>";
        return "Повар $this->name приготовил заказ и передал его оператору $operator->name<br>";
    }
}
class Operator extends Personal
{
    function __construct($name)
    {
        parent::__construct($name);
    }
    public function Order(Customer $customer, Cook $cook)
    {
        echo "Оператор $this->name получает заказ от клиента $customer->name<br>";
        echo "Оператор $this->name отправляет заказ повару $cook->name<br>";
        echo $cook->Order($this);
        return "Оператор $this->name получил готовый заказ от повара $cook->name и отдал его клиенту $customer->name<br>";
    }
}

class Customer
{
    public $name;
    function __construct($name)
    {
        $this->name = $name;
    }
    public function Order($order, Operator $operator, Cook $cook)
    {
        echo "Клиент $this->name делает заказ у оператора $operator->name, который отправляет заказы повару $cook->name<br>Содержимое заказа:<br><br>";
        $num = count($order);
        for ($i = 0; $i < $num; $i++)
        {
            echo $order[$i]->displayInfo();
        }
        echo "<br>";
        echo $operator->Order($this, $cook);
        echo "$this->name доволен";
    }
}
?>