<?php

namespace Transport;

interface CanDeliver
{
    function deliver(Package $weight);
}
class Package
{
    public $name;
    public $weight;
    public function __construct($name, $weight)
    {
        $this->name = $name;
        $this->weight = $weight;
    }
    public function displayInfo()
    {
        echo "Содержимое посылки - $this->name<br>";
        echo "Вес посылки - $this->weight Кг<br>";
    }
}
class Transport implements CanDeliver
{
    public $weight_lift;
    function __construct($weight_lift)
    {
        $this->weight_lift = $weight_lift;
    }
    public function displayInfo()
    {
        echo "Грузоподъемность - $this->weight_lift Кг<br>";
    }
    public function deliver(Package $package){
        if ($this->weight_lift < $package->weight)
        {
            echo "не сможет доставить посылку '$package->name'<br>";
        }
        else
        {
            echo "сможет доставить посылку '$package->name'<br>";
        }
    }
}
class Car extends Transport
{
    public $model;
    function __construct($model, $weight_lift)
    {
        parent::__construct($weight_lift);
        $this->model = $model;
    }
    public function displayInfo()
    {
        echo "Модель автомобиля - $this->model<br>";
        parent::displayInfo();
    }
    public function deliver(Package $package)
    {
        echo "Автомобиль $this->model ";
        parent::deliver($package);
    }
}

class Ship extends Transport
{
    public $model;
    function __construct($model, $weight_lift)
    {
        parent::__construct($weight_lift);
        $this->model = $model;
    }
    public function displayInfo()
    {
        echo "Модель корабля - $this->model<br>";
        parent::displayInfo();
    }
    public function deliver(Package $package)
    {
        echo "Корабль $this->model ";
        parent::deliver($package);
    }
}

class Plane extends Transport
{
    public $model;
    function __construct($model, $weight_lift)
    {
        parent::__construct($weight_lift);
        $this->model = $model;
    }
    public function displayInfo()
    {
        echo "Самолет $this->model терпит крушение<br>";
        parent::displayInfo();
    }
    public function deliver(Package $package)
    {
        echo "Самолет $this->model ";
        parent::deliver($package);
    }
}

?>