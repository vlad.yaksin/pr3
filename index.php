<?php

require_once "transport.php";
use Transport\Car as Car;
use Transport\Ship as Ship;
use Transport\Plane as Plane;
use Transport\Package as Package;

require_once "family.php";
use Family\Grandpa as Grandpa;
use Family\Father as Father;
use Family\Son as Son;

require_once "fastfood.php";
use Fastfood\Burger as Burger;
use Fastfood\Potato as Potato;
use Fastfood\Coffee as Coffee;
use Fastfood\Operator as Operator;
use Fastfood\Cook as Cook;
use Fastfood\Customer as Customer;

echo "Задание 1<br><br>";

$mazda = new Car("Mazda", 50);
$beda = new Ship("__беда", 100);
$broiler = new Plane("Бройлер 747", 20);

$box = new Package("Коробка", 45);
$reactor = new Package("Ядренный реактор", 99);

$box->displayInfo();
echo "<br>";
$reactor->displayInfo();
echo "<br>";
$mazda->displayInfo();
$mazda->deliver($box);
$mazda->deliver($reactor);
echo "<br>";
$beda->displayInfo();
$beda->deliver($box);
$beda->deliver($reactor);
echo "<br>";
$broiler->displayInfo();
$broiler->deliver($box);
$broiler->deliver($reactor);
echo "<br>";

echo "Задание 2<br><br>";

$grandpa = new Grandpa("Иван", "Петров");
$father = new Father("Сергей", $grandpa);
$son = new Son("Вадим", $father);

$grandpa->displayInfo();
echo "<br>";
$father->displayInfo();
echo "<br>";
$son->displayInfo();
echo "<br>";

echo "Задание 3<br><br>";

$burger = new Burger("Гамбургер", true, false);
$potato = new Potato("Картошка-фри", false, false);
$coffee = new Coffee("Кофе", true);

$customer = new Customer("Иван");
$operator = new Operator("Пётр");
$cook = new Cook("Гордон Рамзи");

$customer->Order($order = [$burger, $potato, $coffee], $operator, $cook);

?>