<?php

namespace Family;

class Grandpa
{
    public $first_name_1;
    public $second_name;    
    function __construct($first_name_1, $second_name)
    {
        $this->first_name_1 = $first_name_1;
        $this->second_name = $second_name;
    }
    public function displayInfo()
    {
        echo "$this->first_name_1 $this->second_name<br>";
        echo "Предков нет<br>";
    }
}
class Father extends Grandpa
{
    public $first_name_2;
    public $grandpa;
    public function __construct($first_name_2, $grandpa)
    {
        parent::__construct($grandpa->first_name_1, $grandpa->second_name);
        $this->first_name_2 = $first_name_2;
        $this->grandpa = $grandpa;
    }
    public function displayInfo()
    {
        echo "$this->first_name_2 $this->second_name<br>";
        echo "Его отец - $this->first_name_1 $this->second_name<br>";
    }
}
class Son extends Father
{
    public $first_name_3;
    public $father;
    public
    function __construct($first_name_3, $father)
    {
        parent::__construct($father->first_name_2, $father->grandpa);
        $this->first_name_3 = $first_name_3;
        $this->father = $father;
    }
    public function displayInfo()
    {
        echo "$this->first_name_3 $this->second_name<br>";
        echo "Его отец - $this->first_name_2 $this->second_name<br>";
        echo "Его дед - $this->first_name_1 $this->second_name<br>";
    }
}

?>